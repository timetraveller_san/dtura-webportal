var passed_sems = -1

function enableBtn(){
   document.getElementsByClassName("my_btn")[0].disabled = false;
  }

function make_sliders(n, number){
  if(number == 1){
    $("#calculate1").css("display", "inline");
    container_id = "gpa_sliders1";
  }
  else{
    $("#calculate2").css("display", "inline");
    container_id = "gpa_sliders2";
  }
  slider_html = "";
  for(var i = 1; i < n+1; i++){
    slider_html += "  <form name=\"slider_input\">\
        <h3>Sem: "+ i +"</h3>\
         <input type=\"range\" class=\"slider\" id=\"gpa_s" + i + "\" value=\"5\" min=\"0\" max=\"10\" step=\"0.1\" oninput=\"val_" + i + ".value = gpa_s" + i + ".value \" style=\"display: inline-block;\">\
         <span id=\"rollno\"><input id=\"val_"+ i + "\" value=\"5.0\" style=\"max-width: 50px; color: white;\" oninput=\"gpa_s" + i + ".value = val_" + i + ".value\"></span>\
      </form>";
  }
  passed_sems = n;
  var gpa_sliders = document.getElementById(container_id);
  gpa_sliders.innerHTML = slider_html;
  document.getElementById("result1").innerHTML = "";
  document.getElementById("result2").innerHTML = "";
  return slider_html;
}

function calculate(n, number){
  if(number == 1){
    last_sem = parseFloat(document.getElementById("last_sem_1").value)
    required = parseFloat(document.getElementById("target_gpa1").value)
    if( required == ""){
      return -1
    }
  }
  else{
    last_sem = parseFloat(document.getElementById("last_sem_2").value)
    required = parseFloat(document.getElementById("target_gpa2").value)
    if( required == ""){
      return -1
    }
  }
  left_sems = last_sem - n;
  if(left_sems <= 0 ){
    return -1
  }
  current = 0;
  for(var i = 1; i < n+1; i++){
    var id = "val_" + i;
    slider = document.getElementById(id)
    current += parseFloat(slider.value)
  }
  gpa = (required * last_sem - current)/left_sems
  return gpa
}


$(document).ready(function () {
  document.getElementsByClassName("my_btn")[0].disabled = true;
  $('#sidebarCollapse').click(function (){
      $('#sidebar').toggleClass('active');

  });
  $('#sidebarCollapse2').click(function () {
      $('#sidebar').toggleClass('active');
  });
  document.getElementById("inp").value="Enter roll number (Ex. 2k16/CO/346)";
  $("#inp").focus(function(){
      if(document.getElementById("inp").value === "Enter roll number (Ex. 2k16/CO/346)"){
        document.getElementById("inp").value = "";
      }
    });
  $(".my_btn").click(function(){
      stro = grecaptcha.getResponse()
      var roll = document.getElementById("inp").value.toLowerCase();
      if(roll[1]=='k')
        roll = roll.replace("k","0");
      roll = roll.replace("/","-");
      roll = roll.replace("/","-");
      roll = roll.replace("/","-");
      year = parseInt(roll.slice(1, 4));
      if(stro){
          requestURL='https://timetraveller.pythonanywhere.com/?reg_id=' + roll + "&g-recaptcha-response=" + stro;
          console.log(requestURL);
      }
      else{
        //lets fill some null value in the requestURL to return some error response
        requestURL='https://timetraveller.pythonanywhere.com/?reg_id=2k16-b2-3333'
      }
      //requestURL="data.json";̥
      $.getJSON(requestURL, function(data) {
        if(data['name']===undefined) {
            alert("Captcha not filled or data unavailable. If the problem persists visit 'reform query' page to submit us your data related problem so we can fix it.");
            grecaptcha.reset();
            console.log("Gomenasai! no data present for you desu >///< ");
        }
        else{
          grecaptcha.reset();
          n_sems = data['n_sems'];
          make_sliders(n_sems, 1);
          for(var i = 1; i< n_sems + 1; i++){
            var input_id = "val_" + i;
            var slider_id = "gpa_s" + i
            document.getElementById(input_id).value = data['gpa_sem'+ i + '_sgpa'];
            document.getElementById(slider_id).value = data['gpa_sem'+ i + '_sgpa'];
          }
        }
      });
  });
});


$(document).on('click','#calculate2',function(){
  min_gpa = calculate(passed_sems, 2).toFixed(2);
  if(min_gpa > 10){
    text = "Unachievable. You can't change the past. Sorry."
  }
  else if(min_gpa == -1){
    text = "Wrong set of inputs. Target GPA must not be empty and last semester to count must be greater than semesters passed."
  }
  else{
    text = "Minimum of " + min_gpa + " GPA needed in each of the next " + (parseInt(document.getElementById("last_sem_2").value) - passed_sems) + " semesters. Goodluck!"
  }
  document.getElementById("result2").innerHTML = text;
});

$(document).on('click','#calculate1',function(){
  min_gpa = calculate(passed_sems, 1).toFixed(2);
  if(min_gpa > 10){
    text = "Unachievable. You can't change the past. Sorry."
  }
  else if(min_gpa == -1){
    text = "Wrong set of inputs. Target GPA must not be empty and last semester to count must be greater than semesters passed."
  }
  else{
    text = "Minimum of " + min_gpa + " GPA needed in each of the next " + (parseInt(document.getElementById("last_sem_1").value) - passed_sems) + " semesters. Goodluck!"
  }
  document.getElementById("result1").innerHTML = text;
});

$(document).on('click','#p1',function(){
  make_sliders(1, 2)
});
$(document).on('click','#p2',function(){
  make_sliders(2, 2)
});
$(document).on('click','#p3',function(){
  make_sliders(3, 2)
});
$(document).on('click','#p4',function(){
  make_sliders(4, 2)
});
$(document).on('click','#p5',function(){
  make_sliders(5, 2)
});
$(document).on('click','#p6',function(){
  make_sliders(6, 2)
});
$(document).on('click','#p7',function(){
  make_sliders(7, 2)
});
