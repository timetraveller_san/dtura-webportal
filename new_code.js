//Defining global variables for the use in radar-charts

var sub_grades = [];
var sub_names = [];

function enableBtn(){
   document.getElementsByClassName("my_btn")[0].disabled = false;
  }

function grade_to_number(grade){
  for(var i = 0;i < grade.length;i++){
    grade[i] = grade[i].replace(/\s/g,'');
    if(grade[i]=="O"){
      grade[i]='10';
      grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="A+"){
        grade[i]='9';
        grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="A"){
        grade[i]='8';
        grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="B+"){
        grade[i]='7';
        grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="B"){
        grade[i]='6';
        grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="C"){
        grade[i]='5';
        grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="P"){
        grade[i]='4';
        grade[i]=parseInt(grade[i]);
    }
    else if(grade[i]=="F"){
        grade[i]='0';
        grade[i]=parseInt(grade[i]);
    }
    else{
      grade[i]='0';
      grade[i]=parseInt(grade[i]);
    }
  }
  return grade;
}
function number_to_grade(number){
  switch (number) {
    case 10:
    return 'O'
    break;
    case 9:
    return 'A+'
    break;
    case 8:
    return 'A'
    break;
    case 7:
    return 'B+'
    break;
    case 6:
    return 'B'
    break;
    case 5:
    return 'C'
    break;
    case 4:
    return 'P'
    break;
    default:
    return 'F'

  }
}

function load_chart_1(label_sem,data_rank){
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: label_sem,
          datasets: [{
              label: 'Rank',
              data: data_rank,
              borderColor: [
                  '#26ABDA',
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
}
function load_chart_2(label_sem,data_dr_rank){
  var cty = document.getElementById("myChart2").getContext('2d');
  var myChart = new Chart(cty, {
      type: 'line',
      data: {
        labels: label_sem,
          datasets: [{
              label: 'Dr_Rank',
              data: data_dr_rank,

              borderColor: [
                  '#26ABDA',

              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });

}
function load_chart_3(label_sem,data_gpa){
  var cty2 = document.getElementById("myChart3").getContext('2d');
  var myChart = new Chart(cty2, {
      type: 'doughnut',
      data: {
          labels: label_sem,
          datasets: [{
              label: '# of Votes',
              data: data_gpa,

              borderColor: [
                  '#fff',
                  '#26ABDA',
                  '#77CDEA',
                  '#4BBCE1',
                  '#079FD1',
                  '#037398'
              ],
              backgroundColor: [
                '#fff',
                '#26ABDA',
                '#77CDEA',
                '#4BBCE1',
                '#079FD1',
                '#037398'
              ],
              borderWidth: 1
          }]
      },

  });
}
function load_chart_4(label_sem, data_gpa){
  data_gpa = grade_to_number(data_gpa)
  console.log(label_sem)
  console.log(data_gpa)
  var cty0 = document.getElementById("myChart4").getContext('2d');
  var myChart = new Chart(cty0, {
      type: 'radar',
      data: {
        labels: label_sem,
          datasets: [{
              label: 'Semester Distribution',
              data: data_gpa,
              pointBackgroundColor: '#26ABDA',
              borderColor: ['#26ABDA'],
              borderWidth: 1,
          }]
      },
      options: {
  tooltips: {
    callbacks: {
      label: function(tooltipItem, data_gpa){
        // console.log(data_gpa);
        // console.log(data_gpa.datasets[0].data[1]);
        var grade_no=data_gpa.datasets[0].data[tooltipItem.index];
        var grade_alphabet=number_to_grade(grade_no);
        var string="Grade: "+ grade_alphabet+ " ("+ grade_no+") ";
        return string;
      }
    }
  },
  scale: {
      ticks: {
          beginAtZero: true,
          stepSize: 4,
          showLabelBackdrop: false,
          min:0,
          max: 10
      }
  }
}


  });

}

$(document).ready(function () {
  document.getElementsByClassName("my_btn")[0].disabled = true;
  $("#hidethisyo").hide();
  document.getElementById("inp").value="Enter roll number (Ex. 2k16/CO/346)";
  $("#inp").focus(function(){
    if(document.getElementById("inp").value === "Enter roll number (Ex. 2k16/CO/346)"){
      document.getElementById("inp").value = "";
    }
  });
  $(".my_btn").click(function(){
    stro = grecaptcha.getResponse()
    if(stro){
      $("#hidethisyo").show();
    }
    else{
      //alert("Fill the captcha correctly!");
      // do nothing onii-chan [@_@]
    }
      var roll = document.getElementById("inp").value.toLowerCase();
      if(roll[1]=='k')
        roll = roll.replace("k","0");
      roll = roll.replace("/","-");
      roll = roll.replace("/","-");
      roll = roll.replace("/","-");
      year = parseInt(roll.slice(1, 4));

      if(stro){
          requestURL='https://timetraveller.pythonanywhere.com/?reg_id='+roll+"&g-recaptcha-response="+stro;
          console.log(requestURL);
      }
      else{
        //lets fill some null value in the requestURL to return some error response
        requestURL='https://timetraveller.pythonanywhere.com/?reg_id=2k16-b2-3333'
      }
      //requestURL="data.json";̥
      $.getJSON(requestURL, function(data) {
        console.log(data);
        if(data['name']===undefined) {
            alert("Captcha not filled or data unavailable. If the problem persists visit 'reform query' page to submit us your data related problem so we can fix it.");
            grecaptcha.reset();
            console.log("Gomenasai! no data present for you desu >///< ");
        }
        else{
          $(".main_data").css("display", "block");
          $('html,body').animate({
            scrollTop: $("#start").offset().top},
            1000);
          grecaptcha.reset();

          // $(".main_data").css("opacity", "1");
          var gpa = document.getElementById("gpa");
          var dr_rank = document.getElementById("dr_rank");
          var rank = document.getElementById("rank");
          var name = document.getElementById("name");
          var branch = document.getElementById("branch");
          var roll = document.getElementById("roll_no");
          var gpa_text = document.getElementById("gpa_text");
          var dr_text = document.getElementById("dr_text");
          var rank_text = document.getElementById("rank_text");
          gpa.innerHTML = data['gpa_agg'];
          dr_rank.innerHTML = data['meta_dep_rank_agg'];
          rank.innerHTML = data['meta_uni_rank_agg'];
          branch.innerHTML = data['branch'];
          roll.innerHTML = data['roll'];
          name.innerHTML = data['name'];
          n_sems = data['n_sems']
          label_sem = []
          data_dr_rank = []
          data_rank = []
          data_gpa = []
          gpa_text1 = "( "
          rank_text1 = "( "
          dr_text1 = "( "
          sub_names = []
          sub_grades = []
          $("#drop_sem").html("");
          for(var i = 1; i< n_sems + 1; i++){
            label_sem.push("Sem" + i)
            data_dr_rank.push(data['meta_dep_rank_sem' + i])
            data_rank.push(data['meta_uni_rank_sem' + i])
            data_gpa.push(data['gpa_sem'+ i + '_sgpa'])
            gpa_text1 += data['gpa_sem'+ i + '_sgpa'] + " | "
            rank_text1 += data['meta_uni_rank_sem' + i] + " | "
            dr_text1 += data['meta_dep_rank_sem' + i] + " | "
            sub_names.push(data['sub_names_sem' + i])
            sub_grades.push(data['sub_grades_sem' + i])
            $("#drop_sem").append('<li><a id="dsem' + i + '">Sem'+ i + '</a></li>');
          }
          gpa_text1 = gpa_text1.slice(0, -2) + " )"
          rank_text1 = rank_text1.slice(0, -2) + " )"
          dr_text1 = dr_text1.slice(0, -2) + " )"

          gpa_text.innerHTML = gpa_text1;
          dr_text.innerHTML = dr_text1;
          rank_text.innerHTML = rank_text1;
          // Load charts
          load_chart_1(label_sem, data_rank);
          load_chart_2(label_sem, data_dr_rank);
          load_chart_3(label_sem, data_gpa);
          load_chart_4(sub_names[0], sub_grades[0]);
        }
        $("#hidethisyo").hide();
      });

  });

    $(document).on('click','#dsem1',function(){
      console.log(sub_names[0])
      console.log(sub_grades[0])
      load_chart_4(sub_names[0], sub_grades[0]);
    });
    $(document).on('click','#dsem2',function(){
      load_chart_4(sub_names[1], sub_grades[1]);
    });
    $(document).on('click','#dsem3',function(){
      load_chart_4(sub_names[2], sub_grades[2]);
    });
    $(document).on('click','#dsem4',function(){
      load_chart_4(sub_names[3], sub_grades[3]);
    });
    $(document).on('click','#dsem5',function(){
      load_chart_4(sub_names[4], sub_grades[4]);
    });
    $(document).on('click','#dsem6',function(){
      load_chart_4(sub_names[5], sub_grades[5]);
    });
    $(document).on('click','#dsem7',function(){
      load_chart_4(sub_names[6], sub_grades[6]);
    });
    $(document).on('click','#dsem8',function(){
      load_chart_4(sub_names[7], sub_grades[7]);
    });

    $('#sidebarCollapse').click(function () {
        $('#sidebar').toggleClass('active');
    });
    $('#sidebarCollapse2').click(function () {
        $('#sidebar').toggleClass('active');
    });
});
